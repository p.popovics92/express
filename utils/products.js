const products = [
    {
        productId: "891389f0-4312-42d6-a650-6fda0959c734",
        title: "Book",
        description: "Interesting book",
        price: 200
    },
    {
        productId: "891389f0-0000-42d6-a650-6fda0959c734",
        title: "microvave oven",
        description: "samsung",
        price: 1200
    },
    {
        productId: "891389f0-0000-0000-a650-6fda0959c734",
        title: "laptop",
        description: "lenovo",
        price: 20000
    },
    {
        productId: "891389f0-0000-0000-a650-6fda0959xxxx",
        title: "cheescake",
        description: "no sugar",
        price: 100
    },
    {
        productId: "test",
        title: "TEST-PRODUCT01",
        description: "TEST",
        price: 1
    },
    {
        productId: "test2",
        title: "TEST-PRODUCT02",
        description: "Lorem ipsum dolor sit amnem",
        price: 1000
    },
];


module.exports = products;