const users = [
    {id: 1234},
    {id: 1111},
    {id: 2222},
    {id: 3333},
    {id: 4444},
    {id: 5555},
];

const authUsers = [
    {id: 1234, token: "9999999999", isAdmin: true},
    {id: 1111, token: "0123456789"},
    {id: 2222, token: "9876543210"},
    {id: 3333, token: "1122334455"},
];

module.exports = {users, authUsers};