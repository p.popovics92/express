
const carts = [
    {
        id: 1111,
        deleted: false,
        items: [
            {
                product: {
                    productId: "891389f0-4312-42d6-a650-6fda0959c734",
                    title: "Book",
                    description: "Interesting book",
                    price: 200
                },
                count: 2
            }
        ],
        total: 400
    },
    {
        id: 1111,
        deleted: true,
        items: [
            {
                product: {
                    productId: "891389f0-0000-42d6-a650-6fda0959c734",
                    title: "microvave oven",
                    description: "samsung",
                    price: 1200
                },
                count: 1
            }
        ],
        total: 400
    },
    {
        id: 2222,
        deleted: false,
        items: [
            {
                product: {
                    productId: "891389f0-0000-42d6-a650-6fda0959c734",
                    title: "microvave oven",
                    description: "samsung",
                    price: 1200
                },
                count: 1
            },
            {
                product: {
                    productId: "891389f0-0000-0000-a650-6fda0959c734",
                    title: "laptop",
                    description: "lenovo",
                    price: 20000
                },
                count: 1
            },
            {
                product: {
                    productId: "891389f0-0000-0000-a650-6fda0959xxxx",
                    title: "cheescake",
                    description: "no sugar",
                    price: 100
                },
                count: 2
            },
            
        ],
        total: 21400
    }
]

const paymant = {
        type: "paypal",
        address: "London",
        creditCard: "1234-1234-1234-1234",
        delivery: {
            type: "post",
            address: "London"
        },
        comments: "",
        status: "created",
        total: 0
}



module.exports = {carts, paymant};
