const express = require('express');
const {carts, paymant} = require('./utils/cart');
const products = require('./utils/products');
const { updateCart, deleteCart, permaDeleteCart } = require('./shopping.service');
const authenticateToken = require('./auth.service');

const CARTS = carts;
const PRODUCTS = products;

const app = express();


// ----- {{{{{{{{{{ PARSER }}}}}}}}}} ----- //
const checkHeaders = (req, res, next) => {
    req.allHeaders = {};
    for (const [key, value] of Object.entries(req.headers)) {
        req.allHeaders[key] = value;
    }
    next();
};
app.use(checkHeaders);


// ----- {{{{{{{{{{ AUTH }}}}}}}}}} ----- //
app.use('/api', authenticateToken);


// ----- {{{{{{{{{{ ROUTES }}}}}}}}}} ----- //
// ----- {{{{{{{{{{ GET CART }}}}}}}}}} ----- //
app.get('/api/profile/cart', (req, res) => {
    const shoppingCart = CARTS.filter((cart) => cart.id === req.userId);

    if(shoppingCart) {
        res.status(200).json({ message: shoppingCart});
        console.log(shoppingCart)
    } else {
        res.status(404).json({ error: "Cart was not found..."});
    }
});

// ----- {{{{{{{{{{ MODIFY CART }}}}}}}}}} ----- //
app.put('/api/profile/cart', (req, res ) => {
    const count = Number(req.allHeaders.count);
    let response = updateCart(req.userId, req.allHeaders.productid, count);
    res.status(response.status).json(response.message);
});


// ----- {{{{{{{{{{ DELETE CART }}}}}}}}}} ----- //
app.delete('/api/profile/cart', (req, res) => {
    const cartId = Number(req.allHeaders.cartid);
    if (req.isAdmin) {
        const response = permaDeleteCart(cartId);
        return res.status(response.status).json(response.message); 
    }

    if (cartId === req.userId) {
        const status = deleteCart(req.userId);
        if(status === 204) {
            return res.status(status).json({message: "Cart was deleted successfully."}); 
        } else {
            return res.status(status).json({message: "Cart was not found."}); 
        }
    } else {
        return res.status(404).json({message: "Cart was not found."}); 
    }
});


// ----- {{{{{{{{{{ CHECKOUT CART }}}}}}}}}} ----- //
app.post('/api/profile/cart/checkout', (req, res) => {
    const shoppingCart = CARTS.filter((cart) => {
        return cart.id === req.userId && cart.deleted;
    });

    if(shoppingCart.length) {
        const payedCart = {...shoppingCart, paymant}
        res.status(201).json(payedCart);
    } else {
        res.status(404).json("cart was not found to checkot.")
    }
});


// ----- {{{{{{{{{{ GET ALL PRODUCTS }}}}}}}}}} ----- //
app.get('/api/products', (req, res) => {
    const products = PRODUCTS
    if (products.length) {
        res.status(200).json(products);
    } else {
        res.status(404).json("Something went wrong... there are no product to show.");
    }
});


// ----- {{{{{{{{{{ GET SPECIFIC PRODUCTS }}}}}}}}}} ----- //
app.get('/api/products/:productId', (req, res) => {
    const productId = req.params.productId;

    const product = PRODUCTS.find((product) => product.productId === productId);

    if (product) {
        res.status(200).json(product);
    } else {
        res.status(404).json({ error: "Product not found." });
    }
});

const PORT = 8000;
app.listen(PORT, () => {
    console.log(`server is started on port: ${PORT}`);
});