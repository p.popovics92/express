const CARTS = require("./utils/cart");
const PRODUCTS = require("./utils/products");

const calculatePrice = (cart) => {
    let totalPrice = 0;
    cart.items.forEach((item) => {
        totalPrice += (item.product.price * item.count);
    });
    cart.total = totalPrice;
}

const updateCart = (cartId, newProductId, count) => {
    let response = {
        status: 500, 
        message: "An unexpected error occurred."
    }; 
    let activeCart = CARTS.filter((cart) => {
       return cart.id === cartId && !cart.deleted
    });
    console.log(newProductId)
    const product = PRODUCTS.find((product) => product.productId === newProductId);
    if (!activeCart.length) {
        const newCart ={
                    id: cartId,
                    deleted: false,
                    items: [
                        {
                        product: {
                            productId: product.productId,
                            title: product.title,
                            description: product.description,
                            price: product.price
                        },
                        count: count
                        }
                    ],
                    total: 0
        }
        calculatePrice(newCart);
        CARTS.push(newCart);
        response.status = 201; 
        response.message = "New cart created successfully.";
    } else {
        const productToUpdateIndex = activeCart[0].items.findIndex((item) => {
            const idString = newProductId.toString();
            return item.product.productId === idString;
        });
        console.log(typeof(productToUpdateIndex));
        console.log(productToUpdateIndex);
    
        if (!product) {
            response.status = 404;
            response.message = "Product not found.";
        } else if (count === -1) { 
            if (productToUpdateIndex !== -1) {
                activeCart[0].items.splice(productToUpdateIndex, 1);
                response.status = 200;
                response.message = "Product removed from cart.";
                calculatePrice(activeCart[0]);
            } else {
                response.status = 404;
                response.message = "Product not found in cart.";
            }
        } else if (count > 0) { 
            if (productToUpdateIndex !== -1) {
                activeCart[0].items[productToUpdateIndex].count += count;
                calculatePrice(activeCart[0]);
                response.status = 200;
                response.message = "Cart updated successfully.";
            } else {
                if (product) {
                    activeCart[0].items.push({
                        product: {
                            productId: product.productId,
                            title: product.title,
                            description: product.description,
                            price: product.price
                        },
                        count: count
                    });
                    calculatePrice(activeCart[0]);
                    response.status = 200;
                    response.message = "Product was added to the cart.";
                } else {
                    response.status = 404;
                    response.message = "Product not found.";
                }
            }
        }
    }

    return response;
}

const deleteCart = (cartId) => {
    const response = {};
    const cartToDeleteIndex = CARTS.findIndex((cart) => cart.id === cartId);

    if (cartToDeleteIndex !== -1) {
        CARTS[cartToDeleteIndex].deleted = true;
        response.status = 204;
    } else {
        response.status = 404;
    }

    return response.status;
};


const permaDeleteCart = (cartId) => {
    let response = {
        status: 500, 
        message: "An unexpected error occurred."
    }; 
    
    const cartsToDelete = CARTS.filter((cart) => cart.id === cartId && cart.deleted === true);

    if (cartsToDelete.length > 0) {
        cartsToDelete.forEach((cartToDelete) => {
            const cartToDeleteIndex = CARTS.indexOf(cartToDelete);
            CARTS.splice(cartToDeleteIndex, 1);
        });
        response.status = 204;
        response.message = `All carts with ID ${cartId} and deleted: true were permanently deleted from the database by an admin.`;
    } else {
        response.status = 404;
        response.message = `No carts found with ID ${cartId} and deleted: true.`;
    }
    return response;
}

module.exports = {updateCart, deleteCart, permaDeleteCart};