const { authUsers } = require('./utils/users');

const AUTH_USERS = authUsers;

const authenticateToken = (req, res, next) => {
    console.log(req.allHeaders);

    const authHeader = req.allHeaders.authorization;
    if (!authHeader) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    const token = authHeader.split(' ')[1];
    if (!token) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    const user = AUTH_USERS.find(user => user.token === token);
    if (!user) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    req.userId = user.id;
    if (user.isAdmin) {
        req.isAdmin = user.isAdmin;
    }
    next(); 
};

module.exports = authenticateToken;